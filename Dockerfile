FROM openjdk:11-jdk-buster as builder

RUN apt-get -y update && \ 
    apt-get -y install ant && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /app
COPY . .
RUN ant dist

FROM openjdk:16-jdk-alpine
RUN apk add --no-cache \
    freetype \
    fontconfig \
    ttf-dejavu \
    libxext \
    libxrender \
    libxtst \
    libxi
COPY --from=builder /app/dist/lib/java-assignment-*.jar /java-assignment.jar
CMD java -cp /java-assignment.jar main.Main