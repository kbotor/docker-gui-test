package main;

public class Customer extends Finance_Period {
    
    //Atributes
    private String name;
    private int cNumber, months;
    private double price, amtRepay;
    
    //Constructors
    public Customer (String name, int cNumber, int months, double price) {
        this.name = name;
        this.cNumber = cNumber;
        this.months = months;
        this.price = price;
       
        
    }

    //Getters
    public String getName() {
        return name;
    }

    public int getcNumber() {
        return cNumber;
    }
    
    public int getMonths() {
        return months;
    }
    
     public double getPrice() {
        return price;
    }
     
    //Setter
    public void setName(String name) {
        this.name = name;
    }

    public void setcNumber(int cNumber) {
        this.cNumber = cNumber;
    }

    public void setMonths(int months) {
        this.months = months;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    //Calculation of monthly repayments
    public double calcAmtRepay () {
       
        amtRepay = price / months;
        
        return price / months;
    }
}