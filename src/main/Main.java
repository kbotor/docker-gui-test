package main;

import javax.swing.JOptionPane;


public class Main {

    
    public static void main(String[] args) {
        //Variables
        String name;
        int cNumber, months;
        double price;
        
        //Input
        name = JOptionPane.showInputDialog(null, "Please enter the customer's name:");
        cNumber = Integer.parseInt(JOptionPane.showInputDialog(null, "Please enter the customer's contact number:"));
        price = Double.parseDouble(JOptionPane.showInputDialog(null, "Please enter the price of the product:"));
        months = Integer.parseInt(JOptionPane.showInputDialog(null, "Please enter the number of repayment months:"));
        
        Customer c = new Customer(name, cNumber, months, price);
        
        JOptionPane.showMessageDialog(null, c.calcAmtRepay());
                  
    }

}