# Java Assignment

## Build

### Regular

```shell
ant dist
```

### Docker

```shell
docker build -t java-assignment:latest .
```

## Run

### Regular

```shell
java -cp dist/lib/java-assignment-20200901.jar main.Main
```

### Docker

```shell
docker run -it --rm --env DISPLAY=$DISPLAY --volume="$HOME/.Xauthority:/root/.Xauthority:rw" --net=host java-assignment:latest
```